昨天，女朋友生日完美结束，云原生小程序《睡前故事》的开发也宣布告一段落。  
经过多次迭代，小程序的功能也趋于完善，后续有时间再迭代。  

![](https://p3-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/5df81b04e60f421b89788d0bec37c463~tplv-k3u1fbpfcp-watermark.image)
![](https://p6-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/7cbefa3408bf45a5833dc2d21bd5ca5c~tplv-k3u1fbpfcp-watermark.image)
![](https://p3-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/9346e25e033d49d4ac5a6915d20432bb~tplv-k3u1fbpfcp-watermark.image)
![](https://p6-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/4f5c930e06bf4ab3b83581ab59b7f520~tplv-k3u1fbpfcp-watermark.image)

### 1. 功能介绍

主要包括以下功能。

* 音乐/故事播放
  - 音频播放、进度条拖动、前进后退等
  - 支持循环播放、单曲播放、列表播放
  - 讯飞语音合成、阿里语音合成
  - 故事数据爬取（cheerio + axios + 云函数），数据来源于[情侣故事网](https://www.qlgushi.com/)
  - [网易云音乐](http://musicapi.leanapp.cn/) 免费 API 接口使用
* 收藏功能
  - 收藏音乐/故事列表
  - 收藏音乐/故事单曲
* 查看播放记录
  - 音乐、故事 播放记录
* 动态祝福语
  - 取后台接口，随机展示
* 小程序状态管理
  - 全局状态管理实现历史记录同步刷新功能
  - 已支持全局 store、页面 mixins、页面响应式数据
  - 状态管理代码已开源，可以查看这篇文章 [状态管理解决方案](https://juejin.cn/post/6894489814859513870)
  - 目前仅应用在个人项目中
  
### 2. 项目目录介绍

#### 小程序端项目

![](https://p9-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/c315919a57ec4db28a66535d36c69a11~tplv-k3u1fbpfcp-watermark.image)

主要分为两个主目录。云函数目录和小程序目录。

##### 小程序

* components 通用组件
  - 自定义头部
  - 底部 TabBar
  - 播放列表
* config 配置文件
  - 环境配置
  - 接口配置
  - 页面数据配置
  - 版本控制
* libs
  - 工具函数、工具方法
  - wxs 页面数据处理方法
* data 数据爬取，运行当前文件可以爬取故事数据，自动入库
* images 存放各页面图片资源
* mixins 页面 Mixins，熟悉 Vue 开发的小伙伴应该很熟悉
* models 数据请求模型
* pages 主页面文件
* store 全局仓库配置，目前未使用 modules 形式
* templates 页面通用模板文件

##### 云函数目录

* music 音乐模块
* store 故事模块
* robot 机器人功能模块
  - 目前仅支持语音合成
* service 通用业务功能模块

感觉目前云函数还是有待进一步发展，一些通用的配置无法共享。  
目前我只知晓有两种解决方案：

* 使用 npm 包进行管理通用文件
* 写一个通用的云函数进行调用

貌似官方要推出一种 “层” 的概念，解决此问题，后续再看吧。  
目前一些通用的配置没有抽离，放在各个云函数模块中，感觉上述方案也有些繁琐。

#### 睡前故事后台

![](https://p9-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/b3ae643277344143950eaa0d38c75fc1~tplv-k3u1fbpfcp-watermark.image)

##### 目录介绍

* config 配置文件
  - 通用接口配置文件
  - 秘钥配置文件
* controllers 控制器
* libs 工具目录
  - 工具方法
  - 讯飞语音、阿里语音工具
* routes 请求路由配置
* services 业务层
* views 视图层

后台主要提供语音合成服务，后续打算把数据爬取从云函数迁移至后台，看时间吧。

### 3. 数据表

![](https://p9-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/d54f4144061c448baf9641f93c491383~tplv-k3u1fbpfcp-watermark.image)

数据表用的是云数据库，没啥好说的，按照图片依次创建数据表即可。  
用户可申请免费1个月的资源，后续可以开通按量付费，没啥访问量也花不了多少钱。  
这里只介绍两个表 versioning、blessing。

* versioning 版本控制  
  用于控制首页音乐模块的显示和隐藏
  
  ![](https://p6-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/008d4259bd3742b8b5cd4aca7d2fab72~tplv-k3u1fbpfcp-watermark.image)
  
* blessing 祝福语
  暂时手动添加的，我的页面祝福语。
  
  ![](https://p9-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/92bf267e0b314b4687a3113983ec9594~tplv-k3u1fbpfcp-watermark.image)
  
### 3. 总结

开源不易，切用且珍惜。 

> 友情建议，不要以为有技术你又行了，你还需要买个包，不要问我为啥。

项目体验地址：

![](https://p3-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/686e43f571484cfba5e7201d59814477~tplv-k3u1fbpfcp-watermark.image)
