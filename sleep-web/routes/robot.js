/**
 * @file 路由配置
 * @module routers/xfyun
 * @author 月落 <yueluo.yang@qq.com>
 */

/**
 * @requires node_modules/koa-router 路由模块
 * @requires controllers/xfyun 机器人控制器
 */
const router = require('koa-router')(),
      robotController = require('../controllers/robot')

router.prefix('/robot');

router.post('/tts', robotController.tts);

module.exports = router;
