/**
 * @file 路由配置
 * @module routers/index
 * @author 月落 <yueluo.yang@qq.com>
 */

/**
 * @requires node_modules/koa-router 路由模块
 * @requires controllers/index 首页控制器
 */
const router = require('koa-router')(),
      indexController = require('../controllers/index')

router.get('/', indexController.index);

module.exports = router;
