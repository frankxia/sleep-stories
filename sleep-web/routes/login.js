/**
 * @file 路由配置
 * @module routers/login
 * @author 月落 <yueluo.yang@qq.com>
 */

/**
 * @requires node_modules/koa-router 路由模块
 * @requires controllers/login 登录控制器
 */
const router = require('koa-router')(),
      loginController = require('../controllers/login')

router.prefix('/login');

router.get('/auth', loginController.auth);

module.exports = router;
