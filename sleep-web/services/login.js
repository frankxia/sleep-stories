/**
 * @file 登录业务
 * @module services/login
 * @author 月落 <yueluo.yang@qq.com>
 */

/**
 * @requires log4node - 日志模块
 */
const { RPCClient } = require('@alicloud/pop-core'),
      { aliSecret } = require('../config/secret');

class LoginService {
  /**
   * @description 鉴权
   * @returns {object}
   */
  * auth () {
    const client = new RPCClient({
      accessKeyId: aliSecret.accessKeyId,
      accessKeySecret: aliSecret.accessKeySecret,
      endpoint: 'http://nls-meta.cn-shanghai.aliyuncs.com',
      apiVersion: '2019-02-28'
    });

    const result = yield client.request('CreateToken');

    if (result) {
      return {
        code: 0,
        message: 'request token success.',
        data: {
          token: result.Token
        }
      };
    }

    return {
      code: -1,
      message: 'request token failed.',
      data: {}
    }
  }
}

// 导出登录模块
module.exports = new LoginService();