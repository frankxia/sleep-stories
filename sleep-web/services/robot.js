/**
 * @file 机器人业务
 * @module services/robot
 * @author 月落 <yueluo.yang@qq.com>
 */

/**
 * @requires libs/tts - 语音合成
 */
const tts = require('../libs/tts');
    
class RobotService {
  /**
   * @description 文字转语音
   * @param {object} options - 配置项
   * @param {string} type - 语音类型
   * @returns {promise}
   */
  tts (options, type = 'xfyun') {
    return tts[type](options);
  }
}

// 导出机器人模块
module.exports = new RobotService();