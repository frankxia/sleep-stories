/**
 * @file 首页控制器
 * @module controllers/index
 * @author 月落 <yueluo.yang@qq.com>
 */

/**
 * @requires config/config - 配置文件
 */
const { title } = require('../config/config');

 /**
 * @class Index
 * @classdesc Index控制器
 */
class Index {
  /**
   * @description 渲染首页
   * @return {void}
   */
  * index () {
    yield this.render('index', {
      title
    });
  }
}

// 导出首页控制器
module.exports = new Index();