/**
 * @file 机器人控制器
 * @module controllers/robot
 * @author 月落 <yueluo.yang@qq.com>
 */

/**
 * @requires services/robot - 机器人业务
 */
const robotService = require('../services/robot');

/**
 * @class 机器人
 * @classdesc robot控制器
 */
class Robot {
  /**
   * @description 文字转语音
   * @return {string} 
   */
  * tts () {
    const { id, text } = this.request.body,
          { d_t } = this.request.headers;

    const url = yield robotService.tts({
      d_t,
      id,
      text
    });

    this.body = {
      url
    }
  }
}

// 导出机器人控制器
module.exports = new Robot();