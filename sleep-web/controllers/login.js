/**
 * @file 登录控制器
 * @module controllers/login
 * @author 月落 <yueluo.yang@qq.com>
 */

/**
 * @requires services/login - 登录业务
 */
const loginService = require('../services/login');

/**
 * @class 机器人
 * @classdesc login控制器
 */
class Login {
  /**
   * @description 鉴权
   * @return {object} 
   */
  * auth () {
    const data = yield loginService.auth();
    this.body = {
      data
    }
  }
}

// 导出机器人控制器
module.exports = new Login();