/**
 * @file 基础配置
 * @module config
 * @author 月落 <yueluo.yang@qq.com>
 */

/**
 * @requires path - 原生模块
 */
const path = require('path');

/**
 * @constant {string} title - 标题配置
 */
const title ='睡前故事';

/**
 * @constant {object} hosts - 请求域名
 */
const hosts = {
  aliHost: 'nls-gateway.cn-shanghai.aliyuncs.com',
  xfyunHost: 'tts-api.xfyun.cn'
}

/**
 * @constant {object} domain - 请求地址
 */
const domains = {
  aliDomain: `https://${hosts.aliHost}`,
  xfyunDomain: `wss://${hosts.xfyunHost}`,
  baseDomain: `供前台访问的故事转换音频后的路径`
}

/**
 * @constant {object} outout - 文件输出目录
 */
const outputs = {
  wavSavePath: path.resolve(__dirname, '../public/stories')
}

// 导出配置
module.exports = {
  title,
  hosts,
  domains,
  outputs
}