/**
 * @file 密钥配置
 * @module config/secret
 * @author 月落 <yueluo.yang@qq.com>
 */

/**
 * @constant {object} aliSecret - 阿里语音秘钥配置
 */
const aliSecret = {
  appkey: '###',
  accessKeyId: '###',
  accessKeySecret: '###'
}

/**
 * @constant {object} xfyunSecret - 讯飞语音秘钥配置
 */
const xfyunSecret = {
  appid: '###',
  apiSecret: '###',
  apiKey: '###'
}

// 导出秘钥配置
module.exports = {
  aliSecret,
  xfyunSecret
} 