const app = require('koa')(),
      logger = require('koa-logger'),
      json = require('koa-json'),
      views = require('koa-views'),
      onerror = require('koa-onerror');

const index = require('./routes/index'),
      login = require('./routes/login'),
      robot = require('./routes/robot');
      
// error handler
onerror(app);

// global middlewares
app.use(views('views', {
  root: __dirname + '/views',
  default: 'jade'
}));
app.use(require('koa-bodyparser')({
  enableTypes: ['json', 'form', 'text']
}));
app.use(json());
app.use(logger());

app.use(function *(next){
  let start = new Date;
  yield next;
  let ms = new Date - start;
  console.log('%s %s - %s', this.method, this.url, ms);
});

app.use(require('koa-static')(__dirname + '/public'));

// routes definition
app.use(index.routes(), index.allowedMethods());
app.use(login.routes(), login.allowedMethods());
app.use(robot.routes(), robot.allowedMethods());

// error-handling
app.on('error', (err, ctx) => {
  console.error('server error', err, ctx)
});

module.exports = app;
