/**
 * @file 自定义头部
 * @module components/header
 * @author 月落 <yueluo.yang@qq.com>
 */

Component({
  /**
   * @property {string} mode - 导航栏显示模式
   *  none：无占位元素
   *  active：存在占位元素
   */
  properties: {
    mode: {
      type: String,
      value: 'active'
    }
  },

  /**
   * @property {number} statusBarHeight - 状态栏高度
   * @property {number} navBarHeight - 导航栏高度
   */
  data: {
    statusBarHeight: 0,
    navBarHeight: 0
  },

  lifetimes: {
    attached () {
      // 获取设备信息
      const { statusBarHeight, screenWidth } = wx.getSystemInfoSync();
      // 获取右上胶囊按钮的位置信息
      const { width, height, top, right, bottom } = wx.getMenuButtonBoundingClientRect();

      // 构造胶囊信息
      const capsulePos = {
        width,
        height,
        top: top - statusBarHeight,
        bottom: bottom - height - statusBarHeight,
        right: screenWidth - right
      };
      // 构造导航栏高度
      const navBarHeight =  bottom + capsulePos.top;

      // 设置页面数据
      this.setData({
        statusBarHeight,
        navBarHeight
      });
    }
  }
})
