/**
 * @file 播放列表
 * @module components/playlist
 * @author 月落 <yueluo.yang@qq.com>
 */

/**
 * @requires libs/tools - 工具方法
 */
import tools from '../../libs/tools';

/**
 * @constant {object} - 播放列表类型
 */
const PLAYLIST_MODE = {
  CURRENCY: 'currency',
  BLEND: 'blend'
}

Component({
  /**
   * @property {string} mode - 播放列表类型（通用列表，图文列表）
   * @property {boolean} isShow - 是否显示播放列表
   * @property {string} title - 标题名称
   * @property {array} lists - 播放列表
   * @property {object} collect - 收藏集
   * @property {number} curIdx - 选中下标 
   * @property {boolean} hasTabBar - 是否存在 TabBar 
   */
  properties: {
    mode: {
      type: String,
      value: PLAYLIST_MODE.CURRENCY
    },
    isShow: {
      type: Boolean,
      value: false
    },
    title: {
      type: String,
      value: ''
    },
    lists: {
      type: Array,
      value: []
    },
    collect: {
      type: Object,
      value: {}
    },
    curIdx: {
      type: Number,
      value: 0
    },
    hasTabBar: {
      type: Boolean,
      value: false
    }
  },

  methods: {
    /**
     * @description 关闭播放列表
     * @returns {void}
     */
    close (e) {
      const { action } = e.detail;

      this.triggerEvent('close', { action });
    },

    /**
     * @description 点击事件处理函数
     * @param {object} e - 事件源
     * @returns {void} 
     */
    click (e) {
      const { type, action } = tools.getEventDetail(e),
            { mode, lists, collect } = this.data;

      const item = mode === PLAYLIST_MODE.CURRENCY 
                        ? lists[action]
                        : collect[type][action];

      this.triggerEvent('click', {
        action: type,
        data: Object.assign({
          index: action
        }, item)
      })
    }
  }
})
