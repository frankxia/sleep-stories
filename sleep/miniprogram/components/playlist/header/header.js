/**
 * @file 播放列表 标题
 * @module components/playlist/header
 * @author 月落 <yueluo.yang@qq.com>
 */

Component({
  /**
   * @property {string} title - 标题名称
   */
  properties: {
    title: {
      type: String,
      value: ''
    },
  },

  methods: {
    /**
     * @description 关闭播放列表
     * @returns {void}
     */
    close () {
      this.triggerEvent('close', { action: 'close' });
    }
  }
})
