/**
 * @file tabBar
 * @module components/tabbar/tabbar
 * @author 月落 <yueluo.yang@qq.com>
 */

/**
 * @requires config/tababr - tabBar 配置
 * @requires libs/router - 路由跳转
 */
import { tabbars, TABBARS } from '../../config/tabbar'; 
import router from '../..//libs/router';

// 获取数据仓库
const { $store } = getApp();

Component({
  /**
   * @property {object} tabars - tabBar 配置
   * @property {string} module - 选中模块
   */
  data: {
    tabbars
  },

  lifetimes: {
    attached () {
      this.setData({
        module: $store.state.module
      });
    }
  },

  methods: {
    /**
     * @description 事件处理函数
     * @param {object} e - 事件源 
     * @returns {void}
     */
    onIconClick (e) {
      const { action } = e.currentTarget.dataset,
            { module: selected } = $store.state;

      if (action === selected) return;

      // 提交数据
      $store.commit('changeState', {
        module: action
      });

      // 页面重新加载
      switch (action) {
        case TABBARS.HOME:
          router.relaunch(`/pages/home/home`);
          break;
        case TABBARS.MINE:
          router.relaunch(`/pages/mine/mine`);
          break;
      }
    }
  }
})
