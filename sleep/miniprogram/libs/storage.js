/**
 * @file 本地存储
 * @module libs/storage
 * @author 月落 <yueluo.yang@qq.com>
 */

/**
 * @class Storage
 * @classdesc 用于本地数据存储
 */
class Storage {
  /**
   * @description 获取本地存储的数据
   * @param {string} key - 键值
   * @return {object} 数据集合
   */
  get (key) {
    return wx.getStorageSync(key);
  }

  /**
   * @description 设置数据到本地
   * @param {string} key - 键值
   * @param {object} data - 需要存储的对象
   * @return {void}
   */
  set (key, data) {
    wx.setStorageSync(key, data);
  }

  /**
   * @description 移除指定值
   * @param {string} key - 键值
   * @return {void}
   */
  remove (key) {
    wx.removeStorageSync(key);
  }
}

// 导出本地存储类
export default new Storage();