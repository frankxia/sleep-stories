/**
 * @file 路由跳转
 * @module libs/router
 * @author 月落 <yueluo.yang@qq.com>
 */

/**
 * @description 返回
 * @returns {void}
 */
function goBack () {
  wx.navigateBack({
    delta: 1
  });
}

/**
 * @description 跳转页面
 * @param {string} url - 路由地址
 * @returns {void} 
 */
function navigate (url) {
  wx.navigateTo({
    url
  });
}

/**
 * @description 跳转页面（跳转前关闭所有页面）
 * @param {string} url - 路由地址
 * @returns {void} 
 */
function relaunch (url) {
  wx.reLaunch({
    url
  });
}

export default {
  goBack,
  navigate,
  relaunch
}