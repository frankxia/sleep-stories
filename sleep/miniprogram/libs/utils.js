/**
 * @file 工具方法
 * @module libs/utils
 * @author 月落 <yueluo.yang@qq.com>
 */

/**
 * @description 异步迭代
 * @param {array} data - 数据源 
 * @param {function} fn - 执行函数
 * @param {function} callback - 成功回调函数
 * @returns {promise} 
 */
function asyncForEach (data, fn, callback) {
  return new Promise((resolve, reject) => {
    const total = data.length;

    let count = 0;

    const next = () => {
      if (count < total) {
        return fn(data[count], count++, next);
      }
      callback ? callback(resolve) : resolve();
    }

    next();
  });
}

/**
 * @description 节流函数（事件被触发后，n秒之内只执行一次事件处理函数）
 * @param {function} fn - 回调函数 
 * @param {number} delay - 延时时长
 * @param {boolean} next - 是否执行延时过程中的函数
 * @returns {void}
 */
function throttle (fn, delay, next = true) {
  let begin = new Date().getTime(),
      t = null;

  return function ()  {
    const cur = new Date().getTime();

    t && clearTimeout(t);

    if (cur - begin >= delay) {
      fn && fn.apply(this, arguments);
      begin = cur;
    } else {
      if (next) {
        t = setTimeout(() => {
          fn && fn.apply(this, arguments);
        }, delay);
      }
    }
  }
}

// 导出工具函数
export {
  asyncForEach,
  throttle
}