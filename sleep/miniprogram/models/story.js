/**
 * @file 故事数据模型
 * @module models/story
 * @author 月落 <yueluo.yang@qq.com>
 */

/**
 * @description 爬取故事数据
 * @param {string} field - 故事类型
 * @returns {promise} 
 */
const crawler = (field) => {
  return wx.cloud.callFunction({
    name: 'story',
    data: {
      action: 'crawler',
      data: { field }
    }
  });
}

/**
 * @description 获取主题列表
 * @param {string} field - 主题类型
 * @param {number} page - 页码
 * @param {number} limit - 限制条数
 * @returns {promise}
 */
const getTopicList = (field, page, limit = 10) => {
  return wx.cloud.callFunction({
    name: 'story',
    data: {
      action: 'topic',
      data: { field, page, limit }
    }
  });
}

/**
 * @description 获取主题详情列表
 * @param {string} field - 主题类型
 * @param {number} id - 主题ID 
 * @returns {promise}
 */
const getTopicDetail = (field, id) => {
  return wx.cloud.callFunction({
    name: 'story',
    data: {
      action: 'list',
      data: { field, id, limit: 15 }
    }
  });
}

/**
 * @description 获取故事列表
 * @param {string} field - 主题类型
 * @returns {promise}
 */
const getStoryList = (field) => {
  return wx.cloud.callFunction({
    name: 'story',
    data: {
      action: 'story_list',
      data: { field }
    }
  });
}

/**
 * @description 创建音频映射关系
 * @param {string} id - 故事ID
 * @param {string} url - 故事音频地址
 * @returns {promise} 
 */
const createAudioMapper = (id, url) => {
  return wx.cloud.callFunction({
    name: 'story',
    data: {
      action: 'create_audio',
      data: { id, url }
    }
  });
}

/**
 * @description 获取故事音频地址
 * @param {string} id - 故事ID
 * @returns {promise} 
 */
const getAudioUrl = (id) => {
  return wx.cloud.callFunction({
    name: 'story',
    data: {
      action: 'get_audio',
      data: { id }
    }
  });
}

// 导出故事功能集合
export default {
  crawler,
  getTopicList,
  getTopicDetail,
  getStoryList,
  createAudioMapper,
  getAudioUrl
}