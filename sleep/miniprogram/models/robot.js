/**
 * @file 机器人功能模型
 * @module models/robot
 * @author 月落 <yueluo.yang@qq.com>
 */

/**
 * @description 文字转语音
 * @param {string} id - 故事ID
 * @param {string} text - 故事内容
 * @returns {promise} 
 */
const tts = (id, text) => {
  return wx.cloud.callFunction({
    name: 'robot',
    data: {
      action: 'tts',
      data: { id, text }
    }
  });
}

// 导出机器人功能集合
export default {
  tts
}