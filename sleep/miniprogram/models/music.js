/**
 * @file 音乐数据模型
 * @module models/music
 * @author 月落 <yueluo.yang@qq.com>
 */

/**
 * @requires config/secret - 秘钥配置
 * @requires libs/storage - 本地存储
 * @requires libs/tools - 工具函数
 */
import { musicSecret } from '../config/secret';
import storage from '../libs/storage';
import tools from '../libs/tools';

/**
 * @description 音乐资源初始化
 * @returns {promise}
 */
const init = async () => {
  if (tools.checkUserIsLogin()) return;

  const { result: { code, data } } = await wx.cloud.callFunction({
    name: 'music',
    data: {
      action: 'login',
      data: musicSecret
    }
  });

  if (code === 0) {
    storage.set('user', data);
  }
}

/**
 * @description 获取主题列表
 * @param {string} field - 主题类型
 * @param {number} page - 页码
 * @returns {promise}
 */
const getTopicList = (field, page) => {
  return wx.cloud.callFunction({
    name: 'music',
    data: {
      action: 'topic',
      data: { field, page }
    }
  });
}

/**
 * @description 获取主题详情列表
 * @param {number} id - 主题ID 
 * @returns {promise}
 */
const getTopicDetail = (id) => {
  return wx.cloud.callFunction({
    name: 'music',
    data: {
      action: 'list',
      data: { id, limit: 15 }
    }
  });
}

/**
 * @description 获取音频播放地址
 * @param {number} id - 音频ID
 * @returns {promise} 
 */
const getMusicUrl = (id) => {
  return wx.cloud.callFunction({
    name: 'music',
    data: {
      action: 'url',
      data: { id }
    }
  });
}

// 导出音乐功能集合
export default {
  init,
  getTopicList,
  getTopicDetail,
  getMusicUrl
}