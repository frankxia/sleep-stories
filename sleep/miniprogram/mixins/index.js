/**
 * @file Mixins
 * @module mixins
 * @author 月落 <yueluo.yang@qq.com>
 */

/**
 * @requires mixins/common - 通用 Mixin
 */
import common from './common';

export {
  common
}