/**
 * @file 通用 Mixin
 * @module mixins/common
 * @author 月落 <yueluo.yang@qq.com>
 */

export default {
  data: (ctx) => {
    const { topic } = ctx.$store.state;

    return {
      topic
    }
  }
}