/**
 * @file 初始化
 * @module app
 * @author 月落 <yueluo.yang@qq.com>
 */

/**
 * @requires config/config - 环境配置
 * @requires libs/tools - 工具函数
 * @requires models/music - 音乐功能
 * @requires store - 数据仓库
 */
import config from './config/config';
import tools from './libs/tools';
import musicModel from './models/music';
import store from './store/index';

App({
  $store: store,
  onLaunch: function () {
    if (!tools.checkCloudIsSupport()) {
      return console.error('请使用 2.2.3 或以上的基础库以使用云能力')
    }
    // 初始化云函数
    wx.cloud.init({
      env: config.cloud,
      traceUser: true
    });
    // 初始化音乐配置
    musicModel.init();
  }
})
