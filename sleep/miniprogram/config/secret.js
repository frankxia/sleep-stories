/**
 * @file 秘钥
 * @module config/secret
 * @author 月落 <yueluo.yang@qq.com>
 */

/**
 * @constant {object} musicSecret - 网易云账户及密码
 * @property {string} phone - 手机号
 * @property {string} password - 密码
 */
export const musicSecret = {
  phone: '手机号',
  password: '密码'
}