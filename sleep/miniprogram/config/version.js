/**
 * @file 版本号
 * @module config/version
 * @author 月落 <yueluo.yang@qq.com>
 */

/**
 * @constant {string} - 版本号
 */
export const VERSION = '1.0.9';