/**
 * @file tabBar
 * @module config/tabbar
 * @author 月落 <yueluo.yang@qq.com>
 */

// 导出 TabBar 配置
export const tabbars = [
  {
    field: 'home',
    title: '主页',
    default: '/images/tabbar/home.png',
    selected: '/images/tabbar/home_selected.png'
  },
  {
    field: 'mine',
    title: '我的',
    default: '/images/tabbar/mine.png',
    selected: '/images/tabbar/mine_selected.png'
  }
]

// 导出 TabBar 常量
export const TABBARS = {
  HOME: 'home',
  MINE: 'mine'
}