/**
 * @file 主题配置
 * @module config/topic
 * @author 月落 <yueluo.yang@qq.com>
 */

// 导出主题配置
export const TOPIC = {
  MUSIC: 'music',
  STORY: 'story'
}

// 导出音乐中心主题配置
export const music_center = {
  field: 'cure',
  title: '温暖人心 治愈心灵',
  desc: '清风微起，我喜欢你。',
  url: '/images/topic/cure.png'
} 

// 导出音乐主题配置
export const music_topic = [
  {
    field: 'night',
    title: '夜晚',
    desc: '夜幕降临，万物寂静。',
    coverImgUrl: '/images/topic/night.png'
  },
  {
    field: 'quietness',
    title: '安静',
    desc: '回忆有太多美好。',
    coverImgUrl: '/images/topic/quietness.png'
  },
  {
    field: 'light',
    title: '轻音乐',
    desc: '沉浸下来，静入佳境。',
    coverImgUrl: '/images/topic/light.png'
  },
  {
    field: 'relax',
    title: '放松',
    desc: '伴你放松身心。',
    coverImgUrl: '/images/topic/relax.png'
  },
]

// 导出音乐主题对照
export const music_topic_mapper = {
  'cure': '治愈',
  'night': '夜晚',
  'quietness': '安静',
  'light': '轻音乐',
  'relax': '放松'
}

// 导出故事主题配置
export const story_topic = [
  {
    field: 'sleep',
    title: '睡前故事',
    desc: '又甜又撩的睡前故事',
    coverImgUrl: '/images/topic/sleep.png'
  },
  {
    field: 'fairy',
    title: '童话故事',
    desc: '非常甜的童话故事',
    coverImgUrl: '/images/topic/fairy.png'
  },
  {
    field: 'warm',
    title: '暖心故事',
    desc: '长篇暖心童话故事',
    coverImgUrl: '/images/topic/warm.png'
  },
  {
    field: 'love',
    title: '爱情故事',
    desc: '感动的爱情故事',
    coverImgUrl: '/images/topic/love.png'
  }
]

// 导出故事主题对照
export const story_topic_mapper = {
  'sleep': '睡前故事',
  'fairy': '童话故事',
  'warm': '暖心故事',
  'love': '爱情故事'
}

// 导出主题配置
export const topics = {
  music: music_topic,
  story: story_topic
}

// 导出主题对照配置
export const topci_mappers = {
  music: music_topic_mapper,
  story: story_topic_mapper
}

// 导出操作类型
export const OPEARTION_TYPE = {
  ADD: 'add',
  DELETE: 'delete'
}

// 导出收藏列表类型
export const COLLECT_TYPE = {
  TOPIC: 'topic',
  SINGLE: 'single'
}