/**
 * @file API接口列表
 * @module config/config
 * @author 月落 <yueluo.yang@qq.com>
 */

/**
 * @requires config/env - 项目环境
 */
import { ENV } from './env';

// 导出接口配置
export default {
  test: {
    cloud: 'test-5g8345hne15dc1fc'
  },
  rd: {
    cloud: 'rd-5g8345hne15dc1fc'
  },
  pro: {
    cloud: 'pro-5g8345hne15dc1fc'
  }
}[ENV]