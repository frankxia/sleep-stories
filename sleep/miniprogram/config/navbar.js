/**
 * @file 导航栏
 * @module config/navar
 * @author 月落 <yueluo.yang@qq.com>
 */

// 导出首页导航栏配置
export const index_navbars = [
  {
    field: 'music',
    title: '音乐',
    url: '/images/common/music.png'
  },
  {
    field: 'story',
    title: '故事',
    url: '/images/common/story.png'
  }
]

// 导出主题页导航栏配置
export const topic_navbars = [
  {
    action: 'back',
    url: '/images/common/back.png'
  }
]

// 导出列表页导航栏配置
export const list_navbars = [
  {
    action: 'back',
    url: '/images/common/back.png'
  },
  {
    action: 'collect',
    url: '/images/common/collect.png',
    checked_url: '/images/common/collect_checked.png'
  }
]

// 导出主题页导航栏配置
export const play_navbars = [
  {
    action: 'back',
    url: '/images/common/close.png'
  },
  {
    action: 'collect',
    url: '/images/common/collect.png',
    checked_url: '/images/common/collect_checked.png'
  }
]