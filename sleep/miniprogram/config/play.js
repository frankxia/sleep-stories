/**
 * @file 播放
 * @module config/navar
 * @author 月落 <yueluo.yang@qq.com>
 */

// 导出播放模式图标配置
export const play_mode_icons = [
  {
    field: 'loop',
    title: '循环播放',
    url: '/images/audio/loop.png',
    selected_url: '/images/audio/loop_checked.png'
  },
  {
    field: 'single',
    title: '单曲循环',
    url: '/images/audio/single_cycle.png',
    selected_url: '/images/audio/single_cycle_checked.png'
  },
  {
    field: 'timing',
    title: '定时播放',
    url: '/images/audio/timing.png',
    selected_url: '/images/audio/timing_checked.png'
  },
  {
    field: 'playlist',
    title: '播放列表',
    url: '/images/audio/playlist.png',
    selected_url: '/images/audio/playlist_checked.png'
  }
]

// 导出播放模式配置
export const play_modes = {
  LOOP: 'loop',
  SINGLE: 'single'
}

// 导出播放记录最大限制
export const RECODE_MAX_LIMIT = 50;
