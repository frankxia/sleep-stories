/**
 * @file 运行环境
 * @module config/env
 * @author 月落 <yueluo.yang@qq.com>
 */

/**
 * @constant {string} - 环境配置
 *  test：测试环境
 *  rd：预发布
 *  pro：生产环境
 */
export const ENV = 'test';