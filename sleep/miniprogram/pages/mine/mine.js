/**
 * @file 我的页面
 * @module pages/mine/mine
 * @author 月落 <yueluo.yang@qq.com>
 */

/**
 * @requires models/service - 基础服务
 * @requires libs/tools - 工具方法
 * @requires libs/utils - 工具函数
 * @requires libs/router - 路由工具
 * @requires config/play - 播放配置
 * @requires config/topic - 主题配置
 * @requires reactivity - 状态管理库
 */
import serviceModel from '../../models/service';
import tools from '../../libs/tools';
import { throttle } from '../../libs/utils';
import router from '../../libs/router';
import { RECODE_MAX_LIMIT } from '../../config/play';
import { COLLECT_TYPE } from '../../config/topic';
import { createPage } from '@minipro/reactivity';

createPage()({
  /**
   * @property {array} playRecord - 播放历史
   */
  $data: (ctx) => {
    const { playRecord } = ctx.$store.state;
    
    return {
      playRecord: playRecord.reverse()
    }
  },

  /**
   * @property {string} blessing - 祝福语
   * @property {number} limit - 播放记录最大限制
   * @property {boolean} showRecord - 是否显示播放记录
   * @property {boolean} showCollect - 是否显示收藏集
   * @property {object} collect - 收藏集
   */
  data: {
    blessing: 'loading...',
    limit: RECODE_MAX_LIMIT,
    showRecord: false,
    showCollect: false,
    collect: {}
  },

  onLoad () {
    // 初始化函数
    this.init();
  },

  /**
   * @description 初始化函数
   * @returns {void}
   */
  init () {
    // 获取祝福语
    this.getBlessingWords();
    // 获取收藏集
    this.getCollectList();
  },

  /**
   * @description 获取祝福语
   * @returns {void} 
   */
  async getBlessingWords () {
    const { result } = await serviceModel.getBlessingWords();

    this.setData({
      blessing: result.text
    });
  },

  /**
   * @description 获取收藏集
   * @returns {void}
   */
  async getCollectList () {
    const collect = await serviceModel.getCollectList();

    this.setData({
      collect
    });
  },

  /**
   * @description 事件处理函数
   * @param {object} e - 事件源 
   * @returns {void}
   */
  onClick (e) {
    const { action } = tools.getEventDetail(e);

    switch (action) {
      case 'collect':
      case 'history':
        this.setData({
          [ action === 'collect' ? 'showCollect' : 'showRecord']: true
        });
        break;
      case 'close':
        this.setData({
          showCollect: false,
          showRecord: false
        });
        break;
      default:
        console.log('no matching entries.');
        break;
    }
  },

  /**
   * @description 列表点击事件
   * @param {object} e - 事件源
   * @returns {void} 
   */
  onPlayListClick: throttle(function (e) {
    const { action, data } = tools.getEventDetail(e),
          { topic, type } = data;
  
    const { id } = this.$store.state.playParams;

    if (id === data.id) {
      console.log('禁止切换当前音乐.');
      return;
    }

    // 提交数据
    this.$store.commit('changeState', {
      topic,
      type
    });

    switch (action) {
      case COLLECT_TYPE.TOPIC:
        // 跳转主题列表页
        router.navigate(`/pages/list/list?id=${data.id}`);
        break;
      case COLLECT_TYPE.SINGLE:
        // 更新播放数据
        this.$store.commit('changeState', {
          playParams: data
        })
        // 跳转播放页
        router.navigate(`/pages/play/play`);
        break;
      default:
        console.log('no matching entries.');
        break;
    }

    // 关闭播放列表
    this.setData({
      showCollect: false,
      showRecord: false
    });
  }, 300, false),

  onShareAppMessage () {
    return {
      title: '睡前故事'
    }
  }
})