/**
 * @file 主题页
 * @module pages/list
 * @author 月落 <yueluo.yang@qq.com>
 */

/**
 * @requires config/navbar - 导航配置
 * @requires config/topic - 主题配置
 * @requires models/music - 音乐功能
 * @requires models/story - 故事功能
 * @requires libs/tools - 工具函数
 * @requires libs/router - 路由跳转
 * @requires reactivity - 状态管理库
 */
import { topic_navbars } from '../../config/navbar';
import { TOPIC, topci_mappers, music_topic_mapper } from '../../config/topic';
import musicModel from '../../models/music';
import storyModel from '../../models/story';
import tools from '../../libs/tools';
import router from '../../libs/router';
import { createPage } from '@minipro/reactivity';

createPage()({
  /**
   * @property {string} type - 主题类型
   * @property {string} title - 导航栏名称
   */
  $data: (ctx) => {
    const state = ctx.$store.state;
    
    return {
      topic: state.topic,
      type: state.type,
      title: () => topci_mappers[state.topic][state.type]
    }
  },

  /**
   * @property {object} navbars - 图标导航配置
   * @property {number} navbarHeight - 导航栏高度
   * @property {number} page - 页码
   * @property {array} lists - 列表
   */
  data: {
    navbars: topic_navbars,
    navbarHeight: 0,
    page: 1,
    lists: []
  },

  onLoad () {
    // 初始化
    this.init();
  },

  /**
   * @description 初始化函数
   * @returns {void}
   */
  init () {
    const { topic, type, page } = this.data;

    // 获取导航栏高度
    this.getNavBarHeight();

    switch (topic) {
      case TOPIC.MUSIC:
        // 获取音乐主题列表
        this.getMusicTopicList(type, page);
        break;
      case TOPIC.STORY:
        // 获取故事主题列表
        this.getStoryTopicList(type, page);
        break;
      default:
        break;
    }
  },

  /**
   * @description 获取导航栏高度
   * @returns {void}
   */
  getNavBarHeight () {
    this.setData({
      navbarHeight: tools.getNavBarHeight()
    });
  },

  /**
   * @description 获取音乐主题列表
   * @param {number} topic - 主题类型
   * @param {number} page - 页码
   * @returns {void}
   */
  async getMusicTopicList (topic, page) {
    tools.showLoading();
    const { result } = await musicModel.getTopicList(music_topic_mapper[topic], page);
    tools.hideLoading();

    const { code, message, data } = result;

    if (code === 0) {
      const { lists } = data;

      if (lists.length) {
        this.setData({
          lists
        });
      }
      return;
    }

    console.log(code, message);
  },

  /**
   * @description 获取故事主题列表
   * @param {number} topic - 主题类型
   * @param {number} page - 页码
   * @returns {void}
   */
  async getStoryTopicList (topic, page) {
    tools.showLoading();
    const { result } = await storyModel.getTopicList(topic, page);
    tools.hideLoading();

    const { code, message, data } = result;

    if (code === 0) {
      const { lists } = data;

      if (lists.length) {
        this.setData({
          lists
        });
      }
      return;
    }

    console.log(code, message);
  },
  
  /**
   * @description 导航栏点击事件处理
   * @param {object} e - 事件源
   * @returns {void} 
   */
  onIconBtnClick (e) {
    const { action } = e.currentTarget.dataset;

    switch (action) {
      case 'back':
        router.goBack();
        break;
      default:
        console.log('no matching entries.');
        break;
    }
  },

  /**
   * @description 主题点击处理
   * @param {object} e - 事件源
   * @returns void 
   */
  onClick (e) {
    const { action } = e.currentTarget.dataset;
    // 跳转主题列表页
    router.navigate(`/pages/list/list?id=${action}`);
  }
})