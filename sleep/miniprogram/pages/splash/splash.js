/**
 * @file 加载页
 * @module pages/splash
 * @author 月落 <yueluo.yang@qq.com>
 */

/**
 * @requires libs/router - 路由跳转
 * @requires models/service - 基础服务
 * @requires config/topic - 主题配置
 */
import router from '../../libs/router';
import serviceModel from '../../models/service';
import { TOPIC } from '../../config/topic';

// 获取数据仓库
const { $store } = getApp();

Page({
  /**
   * @property {object} routerTimer - 路由跳转延时器
   */
  routerTimer: null,

  onLoad () {
    // 初始化函数
    this.init();
  },

  /**
   * @description 初始化函数
   * @returns {void}
   */
  init () {
    // 获取版本号
    this.getVersion();
    // 跳转至主页
    this.toHomePage();
  },

  /**
   * @description 获取版本号，控制视图显示
   * @returns {void}
   */
  async getVersion () {
    const { result } = await serviceModel.getVersion(),
          local_version = $store.state.version;

    if (result) {
      const { version } = result;

      if (version === local_version) {
        // 提交数据
        $store.commit('changeState', {
          topic: TOPIC.STORY,
          hideMusic: true
        });
      }
    }
  },

  /**
   * @description 跳转至主页面
   * @returns {void}
   */
  toHomePage () {
    this.routerTimer = setTimeout(() => {
      router.relaunch(`/pages/home/home`);
    }, 3 *  1000);
  },

  /**
   * @description 事件处理函数
   * @returns {void}
   */
  onBtnClick () {
    // 请求延时跳转定时器
    if (this.routerTimer) {
      clearTimeout(this.routerTimer);
    }
    // 跳转至主页面
    router.relaunch(`/pages/home/home`);
  },

  onUnload () {
    // 手动清除定时器
    clearTimeout(this.routerTimer);
  }
})