/**
 * @file 首页
 * @module pages/index
 * @author 月落 <yueluo.yang@qq.com>
 */

/**
 * @requires config/navbar - 导航栏配置
 * @requires config/topic - 主题配置
 * @requires libs/router - 路由跳转
 * @requires reactivity - 状态管理库
 */
import { index_navbars } from '../../config/navbar';
import { topics, music_center } from '../../config/topic';
import router from '../../libs/router';
import { createPage } from '@minipro/reactivity';

createPage()({
  /** 
   * @property {string} topic - 主题
   * @property {boolean} hideMusic - 是否隐藏音乐主题 
   * @property {object} list - 主题列表配置 
   */ 
  $data: (ctx) => {
    const state = ctx.$store.state; 
     
    return {
      topic: state.topic,
      hideMusic: state.hideMusic, 
      list: () => topics[state.topic]
    }
  },

  /**
   * @property {object} navbars - 导航栏配置
   * @property {object} cure - 中心主题配置
   * @property {boolean} loading - 加载中
   */
  data: {
    navbars: index_navbars,
    center: music_center,
    loading: true
  },

  /**
   * @description 导航栏切换
   * @param {object} e - 事件源
   * @returns void 
   */
  onNavbarClick (e) {
    const { field } = e.currentTarget.dataset,
          topic = this.data.topic;
    
    if (topic === field) return;

    // 提交数据
    this.$store.commit('changeState', {
      topic: field
    });
  },

  /**
   * @description 主题点击处理
   * @param {object} e - 事件源
   * @returns void 
   */
  onClick (e) {
    const { action } = e.currentTarget.dataset;

    // 提交数据
    this.$store.commit('changeState', {
      type: action
    });

    // 跳转主题页
    router.navigate(`/pages/topic/topic`);
  },

  onShareAppMessage () {
    return {
      title: '睡前故事'
    }
  }
})
