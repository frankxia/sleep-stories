/**
 * @file 数据仓库配置
 * @module store/index
 * @author 月落 <yueluo.yang@qq.com>
 */

/**
 * @requires reactivity - 状态管理库
 * @requires config/topic - 主题配置
 * @requires config/tabbar - tabBar
 * @requires config/version - 版本号
 */
import { Store } from '@minipro/reactivity';
import { TOPIC } from '../config/topic';
import { TABBARS } from '../config/tabbar'; 
import { VERSION } from '../config/version';

// 导出数据仓库
export default new Store({
  /**
   * @property {string} version - 版本号
   * @property {string} module - 模块
   * @property {string} topic - 主题
   * @property {boolean} hideMusic - 隐藏音乐模块
   * @property {string} type - 主题类型
   * @property {object} playParams - 播放参数
   * @property {object} playList - 播放列表
   * @property {array} playRecord - 历史播放记录
   */
  state: {
    version: VERSION,
    module: TABBARS.HOME,
    topic: TOPIC.MUSIC,
    hideMusic: false,
    type: '',
    playParams: {},
    playList: {
      lists: [],
      total: 0,
      curIdx: 0
    },
    playRecord: []
  },
  mutations: {
    /**
     * @description 更改 store 数据
     * @param {object} state - 原始数据
     * @param {object} data - 当前数据
     * @returns {void}
     */
    changeState: (state, data) => {
      Object.keys(data).forEach(key => (state[key] = data[key]));
    }
  }
});