/**
 * @file 故事数据
 * @module data/data
 * @author 月落 <yueluo.yang@qq.com>
 */

/**
 * @requires models/story - 故事数据模型
 * @requires models/robot - 机器人功能模型
 * @requires config/topic - 爬取配置
 * @requires libs/utils - 工具函数
 */
import storyModel from '../models/story';
import robotModel from '../models/robot';
import { story_topic } from '../config/topic';
import { asyncForEach } from '../libs/utils';

Page({
  onLoad () {
    // 初始化函数
    this.init();
  },

  /**
   * @description 初始化函数
   * @returns {void}
   */
  async init () {
    // 爬取故事数据
    // await this.crawlerStoriesData();
    // 处理音频资源
    // await this.translate();
  },

  /**
   * @description 爬取故事数据
   * @returns {void}
   */
  crawlerStoriesData () {
    return asyncForEach(story_topic, async (topic, index, next) => {
      await storyModel.crawler(topic['field']);
      console.log(`${topic['title']} 抓取完毕.`);
      next();
    });
  },

  /**
   * @description 转换文本为音频资源
   */
  translate () {
    // 转化错误数据
    const error_audio = []; 
    return asyncForEach(story_topic, async (topic, index, next) => {
      // 1. 获取主题故事数据
      const { result } = await storyModel.getStoryList(topic['field']);


      if (result) {
        const { data } = result;

        console.log(topic['field'], data.length);

        await asyncForEach(data, async (story, index, next) => {
          const { id, title, text } = story;

          try {
            console.log(`${id} ${title}`);
            console.log(`-- ${text}`);
  
            // 2. 单篇故事 文本转语音
            const { result } = await robotModel.tts(id, text);
            const { code, message, data: { url } } = result;
  
            if (code === 0) {
              // 3. 创建故事与语音关系映射
              const { result } = await storyModel.createAudioMapper(id, url);
              const { code, message } = result;
              
              console.log(`-- ${message}`);
  
              if (code === 0) {
                // 4. 执行下一次处理， 避免 socket 频繁链接出错，降低处理频率。
                setTimeout(() => next(), 1000 * 2);
                return;
              }

              return;
            }
            
            console.log(message);
            throw new Error(`translate ${title} audio failed.`)
          } catch (error) {
            // 1. 保存错误数据
            error_audio.push(story);
            // 2. 执行下一次合成
            setTimeout(() => next(), 1000 * 5);
          }
        });

        next();
      }
    }, (done) => {
      console.log('数据处理完毕');
      console.log(`error：`, error_audio);
      done();
    });
  }
})