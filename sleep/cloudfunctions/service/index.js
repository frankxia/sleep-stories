/**
 * @file 基础服务
 * @module service
 * @author 月落 <yueluo.yang@qq.com>
 */

/**
 * @requires muisc/service - 基础业务
 */
const service = require('./service');

// 导出函数
exports.main = async (event, context) => {
  const { action, data } = event;
  const result = await service[action](data);
  return result;
}