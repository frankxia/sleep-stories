/**
 * @file 版本控制
 * @module version
 * @author 月落 <yueluo.yang@qq.com>
 */

/**
 * @requires wx-server-sdk - 云函数库
 * @requires config/config - 基础配置
 */
const cloud = require('wx-server-sdk'),
      config = require('../config/config');

// 初始化云函数
cloud.init();
// 数据库实例
const db = cloud.database({
  env: config.env
});

/**
 * @description 获取版本号
 * @returns {object} 
 */
module.exports = async () => {
  const { data: [ { version } ] } = await db.collection(`versioning`).get();
  return {
    version
  }
}