/**
 * @file 基础服务
 * @module /service/index
 * @author 月落 <yueluo.yang@qq.com>
 */

/**
 * @requires service/version - 版本号
 * @requires service/blessing - 祝福话语
 * @requires service/collect - 收藏
 */
const version = require('./version'),
      blessing = require('./blessing'),
      collect = require('./collect');

module.exports = {
  version,
  blessing,
  ...collect
}