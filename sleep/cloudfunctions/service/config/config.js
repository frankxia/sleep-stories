/**
 * @file 基础配置
 * @module version/config
 * @author 月落 <yueluo.yang@qq.com>
 */

// 当前环境
const ENV = {
  test: 'test-5g8345hne15dc1fc',
  rd: 'rd-5g8345hne15dc1fc',
  pro: 'pro-5g8345hne15dc1fc'
}['test'];

// 导出链接配置
module.exports = {
  env: ENV
}