/**
 * @file 网易云音乐功能
 * @module music/service/index
 * @author 月落 <yueluo.yang@qq.com>
 */

/**
 * @requires music/service/login - 登录功能
 * @requires music/service/music - 音乐业务
 */
const login = require('./login'),
      music = require('./music');

module.exports = {
  login,
  ...music
}