/**
 * @file 音乐功能
 * @module music
 * @author 月落 <yueluo.yang@qq.com>
 */

/**
 * @requires muisc/service - 音乐业务
 */
const musicService = require('./service');

// 导出函数
exports.main = async (event, context) => {
  const { action, data = {} } = event;
  const result = await musicService[action](data);
  return result;
}

