/**
 * @file API接口列表
 * @module music/config
 * @author 月落 <yueluo.yang@qq.com>
 */

// 导出接口配置
module.exports = {
  musicDomain: "后台服务音乐地址（参见开源的网易云音乐）"
}