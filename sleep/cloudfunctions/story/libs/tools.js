/**
 * @file 工具方法
 * @module story/libs/tools
 * @author 月落 <yueluo.yang@qq.com>
 */

/**
 * @description 生成UUID
 * @returns {string}
 */
function uuid () {
  let s = [],
      hexDigits = "0123456789abcdef";  

  for (let i = 0; i < 36; i++) {  
    s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);  
  }  

  s[14] = "4";
  s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);
  s[8] = s[13] = s[18] = s[23] = "-";  
 
  return s.join("");  
}

// 导出工具方法
module.exports = {
  uuid
}