/**
 * @file 故事功能
 * @module /service/index
 * @author 月落 <yueluo.yang@qq.com>
 */

/**
 * @requires story/service/crawler - 数据爬取
 * @requires story/service/story - 故事业务
 */
const crawler = require('./crawler'),
      story = require('./story');

module.exports = {
  crawler,
  ...story
}