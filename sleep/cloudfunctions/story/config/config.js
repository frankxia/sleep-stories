/**
 * @file 爬虫配置
 * @module story/config
 * @author 月落 <yueluo.yang@qq.com>
 */

// 当前环境
const ENV = {
  test: 'test-5g8345hne15dc1fc',
  rd: 'rd-5g8345hne15dc1fc',
  pro: 'pro-5g8345hne15dc1fc'
}['test'];

// https://www.qlgushi.com/ 情侣故事网

// 导出链接配置
module.exports = {
  sleep: 'https://www.qlgushi.com/a/3',
  fairy: 'https://www.qlgushi.com/a/33',
  warm: 'https://www.qlgushi.com/a/35',
  love: 'https://www.qlgushi.com/a/36',
  env: ENV
}