/**
 * @file 故事功能
 * @module story
 * @author 月落 <yueluo.yang@qq.com>
 */

/**
 * @requires muisc/service - 故事业务
 */
const storyService = require('./service');

// 导出函数
exports.main = async (event, context) => {
  const { action, data } = event;
  const result = await storyService[action](data);
  return result;
}