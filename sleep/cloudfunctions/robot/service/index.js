/**
 * @file 机器人功能
 * @module robot/service/index
 * @author 月落 <yueluo.yang@qq.com>
 */

/**
 * @requires robot/service/robot - 机器人功能
 */
const robot = require('./robot');

module.exports = {
  ...robot
}