/**
 * @file 机器人功能
 * @module robot/service/robot
 * @author 月落 <yueluo.yang@qq.com>
 */

/**
 * @requires axios - 网络请求库
 * @requires music/config - 接口配置
 */
const axios = require('axios').default,
      config = require('../config/config');

/**
 * @description 文本转音频
 * @param {number} id - 故事ID
 * @param {number} text - 故事内容
 * @returns {object} 
 */
const tts = async ({ id, text }) => {
  try {
    const { data } = await axios({
      url: `${config.sleepDomain}/robot/tts`,
      method: 'POST',
      data: {
        id,
        text
      }
    });
  
    if (data) {
      const { url } = data;
  
      return {
        code: 0,
        message: 'create audio success.',
        data: {
          url
        }
      }
    } else {
      return {
        code: -1,
        message: 'create audio failed.',
        data: {}
      }
    }
  } catch (error) {
    return {
      code: -1,
      message: 'create audio failed.',
      data: {}
    }
  }
}

module.exports = {
  tts
}