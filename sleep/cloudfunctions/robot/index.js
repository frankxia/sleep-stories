/**
 * @file 机器人功能
 * @module robot
 * @author 月落 <yueluo.yang@qq.com>
 */

/**
 * @requires robot/service - 机器人业务
 */
const robotService = require('./service');

// 导出函数
exports.main = async (event, context) => {
  const { action, data = {} } = event;
  const result = await robotService[action](data);
  return result;
}